@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Users</h4>
                        <p class="card-category"> Here you can manage users</p>
                    </div>
                    <div class="card-body">
                        <table id="partnerRequests" class="datatable-table display datatable-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Business Name</th>
                                <th>Business Type</th>
                                <th>Email</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tbody>

{{--                            @if(isset($usersList))--}}
                                @foreach($usersList as $user)
                                    <tr>
                                        <td>{{$user->fullName}}</td>
                                        <td>{{$user->business_name}}</td>
                                        <td>{{$user->business_type}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            <a href="#" class="btn btn-icon btn-primary" data-var="{{$user->id}}" id="accept-partner-{{$user->id}}">
                                                <i class="flaticon2-checkmark"></i>
                                            </a>
                                            <a href="#" class="btn btn-icon btn-danger" data-var="{{$user->id}}" id="reject-partner-{{$user->id}}">
                                                <i class="flaticon2-cancel"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
{{--                            @endif--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="alert alert-danger">
            <span style="font-size:18px;">
              <b> </b> This is a PRO feature!</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            md.initDashboardPageCharts();
        });
    </script>
@endpush
