<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    Route::put('user/activate/{id}','UserController@approveUser' )->name('user.activate');
    Route::get('user/edit/{id}','UserController@editUser' )->name('user.edit');
    Route::delete('user/deactivate/{id}','UserController@rejectUser' )->name('user.deactivate');
    Route::delete('user/delete/{id}','UserController@deleteUser' )->name('user.delete');
});

Route::get('password/activate/{token}', 'Auth\ConfirmAccountController@activate')->name('account.activate');
Route::post('password/confirm-account', 'Auth\ConfirmAccountController@confirm')->name('account.confirm');

//Route::get('language/set/{lang}', "CommonController@setLanguage" )->name('language.set');
Route::get('language/set/{lang}', function ($lang) {
    if (! in_array($lang, ['en', 'ru'])) {
        abort(400);
    }
    \Session::put('locale',$lang);
    return redirect()->back();

    //
})->name('language.set');
//Route::get('send-mail', function () {
//
//    $details = [
//        'title' => 'Mail from ItSolutionStuff.com',
//        'body' => 'This is for testing email using smtp'
//    ];
//
//    \Mail::to('iblees.dev@gmail.com')->send(new \App\Mail\MyTestMail($details));
//
//    dd("Email is Sent.");
//});
//
