@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => __('User management system')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Register') }}</strong></h4>
            <div class="social-line">
                <label rel="tooltip" data-original-title="Applications and camera owner">
                    <input class="flat-radio" type="radio" name="user-type" value="owner" checked>
                    <img src="images/owner.png" alt="Owner">
                </label>

                <label rel="tooltip" data-original-title="Devepoler">
                    <input class="flat-radio" type="radio" name="user-type" value="developer">
                    <img src="images/developer.png" alt="Developer">
                </label>

                <label rel="tooltip" data-original-title="Both variants">
                    <input class="flat-radio" type="radio" name="user-type" value="both">
                    <img src="images/both.png" alt="Both variants">
                </label>
            </div>
          </div>
          <div class="card-body ">

            <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('Name...') }}" value="{{ old('name') }}" required>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif
            </div>
              <div class="bmd-form-group{{ $errors->has('organization') ? ' has-danger' : '' }}">
                  <div class="input-group">
                      <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">supervisor_account</i>
                  </span>
                      </div>
                      <input type="text" name="organization" class="form-control" placeholder="{{ __('Organization...') }}" value="{{ old('organization') }}" required>
                  </div>
                  @if ($errors->has('organization'))
                      <div id="name-error" class="error text-danger pl-3" for="organization" style="display: block;">
                          <strong>{{ $errors->first('organization') }}</strong>
                      </div>
                  @endif
              </div>
              <div class="bmd-form-group{{ $errors->has('country') ? ' has-danger' : '' }}">
                  <div class="input-group">
                      <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">travel_explore</i>
                  </span>
                      </div>
                      <input type="text" name="country" class="form-control" placeholder="{{ __('Country...') }}" value="{{ old('country') }}" required>
                  </div>
                  @if ($errors->has('country'))
                      <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                          <strong>{{ $errors->first('country') }}</strong>
                      </div>
                  @endif
              </div>
              <div class="bmd-form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                  <div class="input-group">
                      <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">home_work</i>
                  </span>
                      </div>
                      <input type="text" name="city" class="form-control" placeholder="{{ __('City...') }}" value="{{ old('city') }}" required>
                  </div>
                  @if ($errors->has('country'))
                      <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                          <strong>{{ $errors->first('city') }}</strong>
                      </div>
                  @endif
              </div>
              <div class="bmd-form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                  <div class="input-group">
                      <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">domain</i>
                  </span>
                      </div>
                      <input type="text" name="address" class="form-control" placeholder="{{ __('Address...') }}" value="{{ old('address') }}" required>
                  </div>
                  @if ($errors->has('address'))
                      <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                          <strong>{{ $errors->first('address') }}</strong>
                      </div>
                  @endif
              </div>
              <div class="bmd-form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                  <div class="input-group">
                      <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">settings_phone</i>
                  </span>
                      </div>
                      <input type="text" name="phone" class="form-control" placeholder="{{ __('Phone...') }}" value="{{ old('phone') }}" required>
                  </div>
                  @if ($errors->has('phone'))
                      <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                          <strong>{{ $errors->first('phone') }}</strong>
                      </div>
                  @endif
              </div>
            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>

            <div class="form-check mr-auto ml-3 mt-3">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="policy" name="policy" {{ old('policy', 1) ? 'checked' : '' }} >
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
                {{ __('I agree with the ') }} <a href="#">{{ __('Privacy Policy') }}</a>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Create account') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
