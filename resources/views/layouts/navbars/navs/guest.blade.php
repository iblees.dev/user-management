<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
  <div class="container">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="{{ route('home') }}">{{ $title }}</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="{{ route('home') }}" class="nav-link">
            <i class="material-icons">dashboard</i> {{ __('Dashboard') }}
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'register' ? ' active' : '' }}">
          <a href="{{ route('register') }}" class="nav-link">
            <i class="material-icons">person_add</i> {{ __('Register') }}
          </a>
        </li>
        <li class="nav-item{{ $activePage == 'login' ? ' active' : '' }}">
          <a href="{{ route('login') }}" class="nav-link">
            <i class="material-icons">fingerprint</i> {{ __('Login') }}
          </a>
        </li>
          <li class="nav-item dropdown">
              <a class="nav-link"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="symbol symbol-20 mr-3">
                  @if(App::getLocale() == 'en')
                          <img src="{{ asset('media/svg/flags/226-united-states.svg') }}" alt=""/>
                      @else
                          <img src="{{ asset('media/svg/flags/013-russia.svg') }}" alt=""/>
                      @endif
                  </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="{{ route('language.set',['lang' => 'en']) }}">
                      <i style="width: 25px; height: 25px; margin-right: 10px;">
                          <img src="{{ asset('media/svg/flags/226-united-states.svg') }}" alt=""/>
                      </i>
                      <span>{{ __('English') }}</span>
                  </a>
                  <a class="dropdown-item" href="{{ route('language.set',['lang' => 'ru']) }}">
                      <i style="width: 25px; height: 25px; margin-right: 10px;">
                          <img src="{{ asset('media/svg/flags/013-russia.svg') }}" alt=""/>
                      </i>
                      <span>{{ __('Russian') }}</span>
                  </a>
              </div>
          </li>
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar -->
