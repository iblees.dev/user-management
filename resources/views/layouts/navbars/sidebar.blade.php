<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->

  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
        @if(Bouncer::is(Auth::user())->an('admin'))
        <li class="nav-item{{$activePage == 'user-management' ? ' active' : ''}}">
            <a class="nav-link" href="{{ route('user.index')  }}">
                <i class="material-icons">dashboard</i>
                <p>{{  __('User Management') }}</p>
            </a>
        </li>
        @endif
    </ul>

  </div>
</div>
