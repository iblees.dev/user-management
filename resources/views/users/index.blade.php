@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Users</h4>
                        <p class="card-category"> Here you can manage users</p>
                    </div>
                    <div class="card-body">
                        <table id="partnerRequests" class="datatable-table display datatable-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Country</th>
                                <th>City</th>
                                <th>Address</th>
                                <th>Organization</th>
                                <th>Full Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>User groups</th>
                                <th>User status</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($usersList))
                                @foreach($usersList as $user)
                                    <tr>
                                        <td>{{$user->country}}</td>
                                        <td>{{$user->city}}</td>
                                        <td>{{$user->address}}</td>
                                        <td>{{$user->organization}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->getRoles()}}</td>
                                        <td>{{$user->status}}</td>
                                        <td>
                                            @if(!Bouncer::is($user)->an('admin'))
                                                @if($user->status == 'new'|| $user->status == 'deactivated')
                                                <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Activate user" data-var="{{$user->id}}" id="activate-user-{{$user->id}}">
                                                    <i class="material-icons">done</i>
                                                </button>
                                                @endif
                                                <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit user" data-var="{{$user->id}}" id="edit-user-{{$user->id}}">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                @if($user->status == 'active')
                                                <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Deactivate user" data-var="{{$user->id}}" id="deactivate-user-{{$user->id}}">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                @endif
                                                @if($user->status == 'new'|| $user->status == 'deactivated')
                                                <button type="button" rel="tooltip" title="" class="btn btn-danger btn-link btn-sm" data-original-title="Remove user" data-var="{{$user->id}}" id="delete-user-{{$user->id}}">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="alert alert-danger">
            <span style="font-size:18px;">
              <b> </b> This is a PRO feature!</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            var PartnersRequestTable = $('#partnerRequests').DataTable({

                rowReorder: false,
                "searching": true,
                "paging": true,
                "bInfo": true,
                "fnDrawCallback": function (oSettings) {

                }
            });
            $('[id^=activate-user-]').on('click', function() {
                var element = $(this);
                if (confirm('Are you sure want to activate?')) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        url: '{{ route('user.activate', ':slug') }}'.replace(':slug', $(this).attr('data-var')),
                        type: 'PUT',
                        success: function(result) {
                            window.location.href = "{{ route('user.index')  }}";
                        }
                    });
                }

            });
            $('[id^=edit-user-]').on('click', function() {
                window.location.href = '{{ route('user.edit', ':slug') }}'.replace(':slug', $(this).attr('data-var'));
            });
            $('[id^=deactivate-user-]').on('click', function() {
                var element = $(this);
                if (confirm('Are you sure want to deactivate?')) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        url: '{{ route('user.deactivate', ':slug') }}'.replace(':slug', $(this).attr('data-var')),
                        type: 'DELETE',
                        success: function(result) {
                            window.location.href = "{{ route('user.index')  }}";
                        }
                    });
                }

            });
            $('[id^=delete-user-]').on('click', function() {
                var element = $(this);
                if (confirm('Are you sure want to delete?')) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        url: '{{ route('user.delete', ':slug') }}'.replace(':slug', $(this).attr('data-var')),
                        type: 'DELETE',
                        success: function(result) {
                            window.location.href = "{{ route('user.index')  }}";
                        }
                    });
                }

            });

            md.initDashboardPageCharts();
        });
    </script>
@endpush
