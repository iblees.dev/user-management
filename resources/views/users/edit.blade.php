@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => __('User Profile')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('user.update',['user'=>$user->id]) }}" autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Edit Profile') }}</h4>
                                <p class="card-category">{{ __('User information') }}</p>
                            </div>
                            <div class="card-body ">
                                @if (session('status'))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                <span>{{ session('status') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Organization') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('organization') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}" name="organization" id="input-name" type="text" placeholder="{{ __('Organization') }}" value="{{ old('organization', $user->organization) }}" required="true" aria-required="true"/>
                                                @if ($errors->has('organization'))
                                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('organization') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Country') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('country') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" id="input-name" type="text" placeholder="{{ __('Organization') }}" value="{{ old('country', $user->country) }}" required="true" aria-required="true"/>
                                                @if ($errors->has('country'))
                                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('country') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('City') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" id="input-name" type="text" placeholder="{{ __('City') }}" value="{{ old('city', $user->city) }}" required="true" aria-required="true"/>
                                                @if ($errors->has('city'))
                                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('city') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Address') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="input-name" type="text" placeholder="{{ __('Address') }}" value="{{ old('city', $user->address) }}" required="true" aria-required="true"/>
                                                @if ($errors->has('address'))
                                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">{{ __('Phone') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="input-name" type="text" placeholder="{{ __('Phone') }}" value="{{ old('city', $user->phone) }}" required="true" aria-required="true"/>
                                                @if ($errors->has('phone'))
                                                    <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" value="{{ old('name', $user->name) }}" required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                                    <div class="col-sm-7">
                                        <label> {{ $user->email }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
