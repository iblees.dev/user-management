<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class UserActivation extends Notification
{
    use Queueable;

    public $token;
    public  $emailReset;
    /**
     * Create a new notification instance.
     * @param  string  $token
     * @param  string  $email
     * @return void
     */
    public function __construct($token, $email)
    {
        $this->token = $token;
        $this->emailReset = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url(route('account.activate', [
            'token' => $this->token,
            'email' => $this->emailReset,
        ], false));

        return (new MailMessage)
            ->subject(Lang::get('Activate User Account Notification'))
            ->line(Lang::get('You are receiving this email because we accepted your request.'))
            ->action(Lang::get('Activate account'), $url)
            ->line(Lang::get('If you did not request for this, no further action is required.'))
            ->line(Lang::get(''))
            ->line(Lang::get('Thanks,'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
