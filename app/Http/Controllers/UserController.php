<?php

namespace App\Http\Controllers;

use App\Notifications\UserActivation;
use App\User;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Bouncer;
use Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        if(Bouncer::is(Auth::user())->an('admin')) {
            $usersList = User::where('status','!=','deleted')->get();
//            var_dump($usersList);die;
            return view('users.index', ['activePage' => 'dashboard', 'users' => $model->paginate(15), 'usersList' => $usersList ] );
        } else {
            abort(403);
        }
    }

    public function deleteUser($id)
    {
        if(Bouncer::is(Auth::user())->an('admin')) {
            User::where('id', $id)
                ->update([
                    'status' => 'deleted'
                ]);
        } else {
            abort(403);
        }
    }

    public function approveUser (Request $request, $id)
    {
        if(Bouncer::is(Auth::user())->an('admin')) {
            $user = User::find($id);
            if ($user->status == 'new') {
                $token = Str::random(64);


                DB::table('password_resets')->insert([
                    'email' => $user->email,
                    'token' => Hash::make($token),
                    'created_at' => Carbon::now()
                ]);
                Notification::route('mail',$user->email)->notify(new UserActivation($token, $user->email));
            }
            $user->status = 'active';
            $user->save();

        } else {
            abort(403);
        }
    }

    public function rejectUser($id)
    {
        if(Bouncer::is(Auth::user())->an('admin')) {
        User::where('id', $id)
            ->update([
                'status' => 'deactivated'
            ]);
        } else {
            abort(403);
        }
    }

    public function editUser($id)
    {
        if(Bouncer::is(Auth::user())->an('admin')) {
            $data['user'] = User::find($id);
            return view('users.edit', $data);
        } else {
            abort(403);
        }
    }

    public function update(Request $request, $id)
    {
        if(Bouncer::is(Auth::user())->an('admin')) {
            $user = User::find($id);
            $user->name = $request->name;
            $user->organization = $request->organization;
            $user->country = $request->country;
            $user->city = $request->city;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->save();
            return back()->with('message', 'Record Successfully Updated!');
         } else {
            abort(403);
        }
    }

}
