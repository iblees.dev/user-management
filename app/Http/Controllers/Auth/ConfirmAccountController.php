<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ConfirmAccountController extends Controller
{


    public function activate(Request $request, $token)
    {

        $data['token'] = $token;
        $data['email'] = $request->email;
        return view('auth.passwords.activate', $data);

    }

    public function confirm(Request $request)
    {
        $token = $request->token;
        $email = $request->email;
        $password = $request->password;
        $resetData = DB::table('password_resets')->where('email', $email)->first();
        if ( Hash::check( $token,$resetData->token) ) {
            $user = User::whereEmail($resetData->email)->first();
            $user->password = Hash::make($password);
            $user->save();
            Auth::guard()->login($user);
            DB::table('password_resets')->where('email', $email)->delete();
            return redirect()->route('home');
        }
    }
}
