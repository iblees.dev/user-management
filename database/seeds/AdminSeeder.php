<?php

use Illuminate\Database\Seeder;
use app\User;
use Silber\Bouncer\BouncerFacade as Bouncer;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = User::create([
            'name' => 'Admin',
            'country' => 'Admin country',
            'city' => 'Admin city',
            'address' => 'Admin address',
            'organization' => 'Admin organization',
            'phone' => '88000000000',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456')
        ]);

        $adminUser->assign('admin');
    }
}
